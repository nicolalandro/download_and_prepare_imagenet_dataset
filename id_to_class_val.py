import os
import json

folder = './val'
train_dirs = os.listdir(folder)
with open('./imagenet_class_index.json') as jsonfile:
    data = json.load(jsonfile)
id_to_class = dict()
for i,(n, s) in data.items():
    s_clean = s.replace("\'", "").replace(" ", "").replace("(", "_").replace(")", "").strip()
    id_to_class[n] = f'{i.zfill(3)}-{s_clean}'

train_dirs = list(filter(lambda x: x.startswith('n'), train_dirs))
for d in train_dirs:
    to_rename_folder = os.path.join(folder, d)
    new_name_folder = os.path.join(folder, id_to_class[d])
    command = f'mv {to_rename_folder} {new_name_folder}'
    print(command)
    os.system(command)

